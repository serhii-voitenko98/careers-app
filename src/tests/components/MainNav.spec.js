import { mount } from '@vue/test-utils';
import MainNav from '@/components/MainNav';

describe('MainNav component', () => {
  it('should display company name', () => {
    const wrapper = mount(MainNav);
    expect(wrapper.text()).toMatch('Careers');
  });

  it('should display menu items for navigation', () => {
    const wrapper = mount(MainNav);
    const navigationMenuItems = wrapper.findAll('[data-test="main-nav-list-item"]');
    const navigationMenuTexts = navigationMenuItems.map((item) => item.text());
    expect(navigationMenuTexts).toEqual(['Teams', 'Locations', 'Life at Careers', 'How we hire', 'Students', 'Jobs']);
  });

  describe('when user is logged out', () => {
    it('should prompts user to sign in', () => {
      const wrapper = mount(MainNav);
      const loginButton = wrapper.find('[data-test="login-button"]');
      expect(loginButton.exists()).toBe(true);
    });
  });

  describe('when user is logged in', () => {
    it('should display user profile image', async () => {
      const wrapper = mount(MainNav);
      let profileImage = wrapper.find('[data-test="profile-image"]');
      expect(profileImage.exists()).toBe(false);

      let loginButton = wrapper.find('[data-test="login-button"]');
      await loginButton.trigger('click');

      profileImage = wrapper.find('[data-test="profile-image"]');
      loginButton = wrapper.find('[data-test="login-button"]');

      expect(profileImage.exists()).toBe(true);
      expect(loginButton.exists()).toBe(false);
    });
  });
});
